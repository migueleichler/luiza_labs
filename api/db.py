from pymongo import MongoClient


def connect_database(app):
    """
        Cria uma nova conexão com o MongoDb
    """
    client = MongoClient(host='localhost')
    app.db = client['luiza_labs']
