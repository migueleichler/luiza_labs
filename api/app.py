from flask import Flask, Blueprint
from flask_restful import Api

from .routes import generate_routes
from .db import connect_database


bp_api = Blueprint('api_luiza_labs', __name__)
api = Api(bp_api)


def create_app():
    """
        Cria o app Flask
    """
    app = Flask(__name__)
    app.register_blueprint(bp_api, url_prefix='/api')

    generate_routes(api)
    connect_database(app)

    return app
