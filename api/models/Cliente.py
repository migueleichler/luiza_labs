from marshmallow import Schema, fields


class Cliente(Schema):
    nome = fields.String(
        required=True,
        error_messages={'required': {'message': 'O Nome é obrigatório'}}
    )
