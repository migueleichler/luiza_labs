import unittest
from unittest.mock import patch

from api.app import create_app


class TestViewCliente(unittest.TestCase):

    def setUp(self):
        self.client = create_app().test_client()
        self.client.testing = True

        self.cliente_id = 'aaa111'
        self.clientes = [{'id': self.cliente_id, 'nome': 'Manoel', 'email': 'manoel@email.com'}]

    @patch("pymongo.collection.Collection.aggregate")
    def test_get_clientes(self, mock_aggregate):
        mock_aggregate.return_value = self.clientes
        response = self.client.get('/api/cliente/')

        self.assertEqual(response.status_code, 200)

    @patch("pymongo.collection.Collection.aggregate")
    def test_get_clientes_response(self, mock_aggregate):
        mock_aggregate.return_value = self.clientes
        response = self.client.get('/api/cliente/')

        self.assertEqual('cliente' in response.get_json().keys(), True)
        self.assertEqual('nome' in response.get_json()['cliente'][0].keys(), True)
        self.assertEqual('email' in response.get_json()['cliente'][0].keys(), True)
        self.assertEqual('id' in response.get_json()['cliente'][0].keys(), True)

    @patch("pymongo.collection.Collection.find_one")
    def test_get_cliente_by_id(self):
        url = '/api/cliente/{}/'.format(self.cliente_id)
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)

    @patch("pymongo.collection.Collection.find_one")
    def test_get_cliente_by_id_response(self, mock_find_one):
        mock_find_one.return_value = self.clientes
        url = '/api/cliente/{}/'.format(self.cliente_id)
        response = self.client.get(url)
        print(response.get_json())

        self.assertEqual('cliente' in response.get_json().keys(), True)
        self.assertEqual('nome' in response.get_json()['cliente'][0].keys(), True)
        self.assertEqual('email' in response.get_json()['cliente'][0].keys(), True)
        self.assertEqual('id' in response.get_json()['cliente'][0].keys(), True)


if __name__ == '__main__':
    unittest.main()
