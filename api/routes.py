from api.views.Cliente import ClienteListView, ClienteView


def generate_routes(api):
    # Cliente
    api.add_resource(ClienteListView, '/cliente/')
    api.add_resource(ClienteView, '/cliente/<cliente_id>/')
