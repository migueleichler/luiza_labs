from flask_restful import Resource
from flask import current_app as app
from flask import jsonify
from bson.objectid import ObjectId


class ClienteListView(Resource):

    def get(self):
        aggregation = [{'$project': {'_id': 0, 'nome': 1, 'id': {'$toString': '$_id'}}}]
        clientes = list(app.db['cliente'].aggregate(aggregation))

        return jsonify({'cliente': clientes})


class ClienteView(Resource):

    def get(self, cliente_id):
        cliente_dict = {}
        cliente = app.db['cliente'].find_one({'_id': ObjectId(cliente_id)}, {'_id': 0})
        if cliente:
            cliente['id'] = cliente_id
            for key, value in cliente.items():
                cliente_dict[key] = value

        return {'cliente': cliente_dict}
