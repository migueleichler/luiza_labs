create_venv:
	python3 -m venv .venv
	pip3 install -r requirements.txt

test:
	python3 -m unittest discover
